### Just Another Bulk Renamer
***
A simple and extensible bulk-renaming tool inspired by Thunar's Bulk Renamer.

### Creating Scripts
***
JABR is extensible. It works by searching for `.py` and `.pyc` files from the `scripts` directory and importing them.
The script's functions will then be called by JABR whenever it is needed.

When creating a script for JABR, it **must** have the following functions:

- `init(jabr, update_newname)` - This is called whenever your script is selected. Its' primary use is to provide JABR with the UI of your script.
 It has two parameters: *jabr* and *update_newname*. Use *jabr* to place your UI elements on JABR,
 and *update_newname* to let your UI elements call on the *update_newname* function of the application.
- `update_filename(string, file)` - This is called whenever JABR creates a preview of the output of your script in the list of new filenames.
 It takes a *string* argument, which is the filename and *file* which points to the file's location. This function should contain the process of changing the filename.

There is another function that you may use on your script, although it is only optional:

- `cleanup()` - You may use this function to clean-up after *update_filename* is called.
 JABR will check if your script contains this function, but won't require it.

You may check the sample scripts that comes with JABR, as examples to creating your own script/s.

### Building a Windows executable
***
In order to build it for Windows, you will need to have the following installed:

- Python 2.6 or higher

- cx_Freeze (must correspond to your version of Python)

With the above installed, simply navigate to the source directory and run this command using your command line: `python setup.py build`.