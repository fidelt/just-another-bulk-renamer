# Date script for JABR
# Author: Fidel Tamondong
# License: GNU General Public License v3

from calendar import monthrange
import datetime
import os
import sys

if (sys.version_info[0] >= 3):
  # Python 3 or greater
  import tkinter
else:
  import Tkinter as tkinter
  
def init(jabr, update_newname):
  init.rename = 1
  init.update_newname = update_newname
  
  # Gets current date
  date_today = datetime.datetime.now()
  
  datetype_label = tkinter.Label(jabr, text='Date:')
  datetype_label.grid(column=0, row=0, sticky='e')
  init.datetype_options = ['Current', 'Custom', 'File Creation', 'File Modified']
  init.datetype = tkinter.StringVar()
  init.datetype.set(init.datetype_options[0])
  datetype_optionmenu = tkinter.OptionMenu(
      jabr,
      init.datetype,
      *init.datetype_options,
      command=datetype_change
      )
  datetype_optionmenu.configure(width=20)
  datetype_optionmenu.grid(column=1, row=0, columnspan=3, sticky='w')
  
  dateformat_label = tkinter.Label(jabr, text='Format:')
  dateformat_label.grid(column=4, row=0, sticky='e')
  init.dateformat_entry = tkinter.Entry(jabr, width=1)
  init.dateformat_entry.grid(column=5, row=0, sticky='ew')
  init.dateformat_entry.bind("<KeyRelease>", update_newname)
  init.dateformat_entry.insert(0, '%Y-%m-%d')
  
  init.year = tkinter.StringVar()
  init.year.set(date_today.year)
  year_label = tkinter.Label(jabr, text='Year:')
  year_label.grid(column=0, row=1, sticky='e')
  init.year_spinbox = tkinter.Spinbox(
      jabr,
      width=6,
      from_=1970,
      to=9999,
      state='disabled',
      textvariable=init.year,
      command=setmaxdays
      )
  init.year_spinbox.grid(column=1, row=1, sticky='w')
  init.year_spinbox.bind("<KeyRelease>", checkdate)
  
  init.month = tkinter.StringVar()
  init.month.set(date_today.month)
  month_label = tkinter.Label(jabr, text='Month:')
  month_label.grid(column=2, row=1, sticky='e')
  init.month_spinbox = tkinter.Spinbox(
      jabr,
      width=6,
      from_=1,
      to=12,
      state='disabled',
      textvariable=init.month,
      command=setmaxdays
      )
  init.month_spinbox.grid(column=3, row=1, sticky='w')
  init.month_spinbox.bind("<KeyRelease>", checkdate)
  
  currentmonthdays = monthrange(date_today.year, date_today.month)[1]
  init.day = tkinter.StringVar()
  init.day.set(date_today.day)
  day_label = tkinter.Label(jabr, text='Day:')
  day_label.grid(column=4, row=1, sticky='e')
  init.day_spinbox = tkinter.Spinbox(
      jabr,
      width=6,
      from_=1,
      to=currentmonthdays,
      state='disabled',
      textvariable=init.day,
      command=update_newname
      )
  init.day_spinbox.grid(column=5, row=1, sticky='ew')
  init.day_spinbox.bind("<KeyRelease>", checkdate)
  
  filename_label = tkinter.Label(jabr, text='Filename:')
  filename_label.grid(column=0, row=2, sticky='e')
  init.filename_options = ['Date Filename', 'Filename Date']
  init.filename = tkinter.StringVar()
  init.filename.set(init.filename_options[0])
  filename_optionmenu = tkinter.OptionMenu(
      jabr,
      init.filename,
      *init.filename_options,
      command=update_newname
      )
  filename_optionmenu.grid(column=1, row=2, columnspan=5, sticky='ew')
  
  jabr.grid_columnconfigure(5, weight=1)
  
def update_filename(string, file):
  if init.rename == 0:
    return
  
  datetype_index = init.datetype_options.index(init.datetype.get())
  filename_index = init.filename_options.index(init.filename.get())
  
  date = datetime.datetime.now()
  if datetype_index == 0:
    date = datetime.datetime.now()
  elif datetype_index == 1:
    year = init.year_spinbox.get()
    month = init.month_spinbox.get()
    day = init.day_spinbox.get()
    date = datetime.datetime.strptime(year + month + day, '%Y%m%d')
  elif datetype_index == 2:
    date = datetime.datetime.fromtimestamp(os.path.getctime(file))
  elif datetype_index == 3:
    date = datetime.datetime.fromtimestamp(os.path.getmtime(file))
  
  try:
    date = date.strftime(init.dateformat_entry.get())
  except ValueError:
    pass
  
  if filename_index == 0:
    return date + string
  else:
    return string + date
  
def checkdate(evt):
  if not evt.widget.get().isdigit():
    invalid_input(evt.widget)
    return
  
  value = float(evt.widget.get())
  maxval = evt.widget.config()['to'][4]
  minval = evt.widget.config()['from'][4]
  if (value > maxval or value < minval):
    invalid_input(evt.widget)
    return
  
  evt.widget.config(bg='white')
  setmaxdays()
  
def invalid_input(widget):
  widget.config(bg='red')
  init.rename = 0
  init.update_newname()
  
def setmaxdays():
  if not init.year_spinbox.get().isdigit() or not init.month_spinbox.get().isdigit():
    init.rename = 0
    return
  
  year = int(init.year_spinbox.get())
  month = int(init.month_spinbox.get())
  days_in_month = monthrange(year, month)[1]
  init.day_spinbox.config(to=days_in_month)
  init.rename = 1
  init.update_newname()

def datetype_change(datetype):
  # If datetype != 'custom'
  if (datetype is not init.datetype_options[1]):
    init.year_spinbox.config(state='disabled')
    init.month_spinbox.config(state='disabled')
    init.day_spinbox.config(state='disabled')
  else:
    init.year_spinbox.config(state='normal')
    init.month_spinbox.config(state='normal')
    init.day_spinbox.config(state='normal')
  init.update_newname()